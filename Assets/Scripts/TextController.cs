﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class TextController : MonoBehaviour {

    public Text text;
    public Text title;
    public Text infoText;

    private SceneList sl;
    private Scene s;
    private HashSet<string> gameState = new HashSet<string>();

    // Use this for initialization
    void Start() {
        LoadZone("intro");
        s = sl.Find("openingscreen");
    }

    // Update is called once per frame
    void Update() {
        if (!s.title.Equals("")) {
            title.text = s.title;
        }

        string addText = "";
        foreach (OptionalText t in s.optTexts) {
            if (t.Check(gameState)) {
                addText += t.text + " ";
            }
        }
        text.text = String.Format("{0}\n\n{1}", s.text, addText);

        string info = "";
        foreach (SceneAction a in s.actions) {
            if (a.Check(gameState)) {
                info += String.Format("({0}) {1} ", a.trigger.ToUpper(), a.info);
            }
        }
        infoText.text = info;

        foreach (SceneAction a in s.actions) {
            if (a.Check(gameState) && Input.GetKeyDown(a.trigger)) {
                a.Apply(gameState);
                if (a.resetState) {
                    gameState.Clear();
                }
                if (!a.loadZone.Equals("")) {
                    LoadZone(a.loadZone);
                }
                s = sl.Find(a.target);
                break;
            }
        }
    }

    void LoadZone(string zone) {
        sl = JsonUtility.FromJson<SceneList>(Resources.Load<TextAsset>(zone).text);
    }
}

#region Scene file support

[Serializable]
public class SceneList {
    public string zoneId;
    public Scene[] scenes;

    public Scene Find(string findId) {
        foreach (Scene scene in scenes) {
            if (scene.id.Equals(findId)) {
                return scene;
            }
        }
        return null;
    }
}

[Serializable]
public class Scene {
    public string id;
    public string title = "";
    public string text;
    public OptionalText[] optTexts = new OptionalText[0];
    public SceneAction[] actions = new SceneAction[0];
}

[Serializable]
public class OptionalText {
    public string text;
    public string[] checkTrue = new string[0];
    public string[] checkFalse = new string[0];

    public bool Check(HashSet<string> s) {
        foreach (string cond in checkTrue) {
            if (!s.Contains(cond)) {
                return false;
            }
        }
        foreach (string cond in checkFalse) {
            if (s.Contains(cond)) {
                return false;
            }
        }
        return true;
    }
}

[Serializable]
public class SceneAction {
    public string trigger;
    public string target;
    public string info;
    public string[] checkTrue = new string[0];
    public string[] checkFalse = new string[0];
    public string[] setTrue = new string[0];
    public string[] setFalse = new string[0];
    public string loadZone = "";
    public bool resetState = false;

    public bool Check(HashSet<string> s) {
        foreach (string cond in checkTrue) {
            if (!s.Contains(cond)) {
                return false;
            }
        }
        foreach (string cond in checkFalse) {
            if (s.Contains(cond)) {
                return false;
            }
        }
        return true;
    }

    public void Apply(HashSet<string> s) {
        foreach (string cond in setTrue) {
            s.Add(cond);
        }
        foreach (string cond in setFalse) {
            s.Remove(cond);
        }
    }
}

#endregion